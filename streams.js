var clientID = process.env.TWITCH_CLIENT_ID;
var req = require("request");
var queryString = require("querystring");

function Streams() {
	// request headers
	var options = {
		headers: {
			"Accept": "application/vnd.twitchtv.v5+json",
			"Client-ID": clientID
		}
	}

	// retrieve the top 10 streamers of a specific game
	this.topTen = function(game, callback) {
		var game = queryString.escape(game);
		// url request to get streams of a particular game limited by 10
		options["url"] = "https://api.twitch.tv/kraken/streams/?game=" + game + "&limit=10";

		req(options, function(err, res, body) {
			var body = JSON.parse(body);
			// the request returns a 200 even if the game does not exist. Must check the total key for 0 streams
			if(body._total == 0) {
				callback("The game does not exist or there is no one online")
			} else {
				var names = [];
				for(var i = 0; i < body.streams.length; i++) {
					names.push(body.streams[i].channel.name);
				}
				callback(names);
			}
		})
	}

	// retrieve the featured stream from the homepage
	this.featured = function(callback) {
		options["url"] = "https://api.twitch.tv/kraken/streams/featured?limit=10";
		req(options, function(err, res, body) {
			var names = [];
			var feature;
			var body = JSON.parse(body);

			for(var i = 0; i < body.featured.length; i++) {
				// console.log(body.featured[1])
				feature = {
					name: body.featured[i].stream.channel.name,
					game: body.featured[i].stream.game
				}
				names.push(feature);
			}
			callback(names)
		})
	}
}

module.exports = new Streams()